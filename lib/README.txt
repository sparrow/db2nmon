此目录保存的是各个监控功能模块。
功能模块分为如下三大类：
1）、系统级别监控。
	包括：osstat_CPU/osstat_MEM/osstat_NIC/osstat_LOAD/osstat_IO.
	这些脚本中会判断操作系统版本，然后不同的版本做不同的处理。
2）、DB2数据库级别监控。
	包括：db2V95/db2V97/db2V101/db2V105
	这些脚本中会判断操作系统版本，然后不同的版本做不同的处理。

注意：
本目录中的脚本都是可以执行的，脚本的模板为：

#!/bin/bash
#description: this is a simple scipt.

SCRIPTNAME=`basename $0`
OSVER=$1
DB2VER=$2
DBNAME=$3
WPATH=$4

TMPPATH="$WPATH/var/log/db2nmon"
STATFILE="$WPATH/var/lock/db2nmon/$SCRIPTNAME"

function Get_CPU_MEM_stat () {
	local return_value=0
	...
	return $return_value
}

#main Function.
Get_CPU_MEM_stat
if [ $? -ne 0 ];then
	echo 1 >$STATFILE
else
	echo 0 >$STATFILE
fi
#End.
